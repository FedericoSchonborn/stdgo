# `stdgo`

**(PoC)** Rust bindings for the Go standard library (via CGo).

## Example

```rust
use stdgo::{fmt, time};

fn main() {
    fmt::println(&format!(
        "Today's date is {}!",
        time::Time::now().format("Monday January 2, 2006"),
    ));
}
```

## TODO

- [ ] Implement `cgobindgen` to generate the `go-sys` bindings automagically.
