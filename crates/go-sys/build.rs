use std::{env, fs, path::PathBuf, process::Command};

fn main() {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
    let library_path = out_dir.join("libgo-sys.so");
    let header_path = out_dir.join("libgo-sys.h");
    let bindings_path = out_dir.join("bindings.rs");

    println!("cargo:rerun-if-changed=cgo");
    println!("cargo:rustc-link-search={}", out_dir.display());
    println!("cargo:rustc-link-lib=go-sys");

    Command::new("go")
        .arg("build")
        .arg("-v")
        .arg("-x")
        .arg("-trimpath")
        .arg("-buildmode=c-shared")
        .arg("-o")
        .arg(&library_path)
        .arg("./cgo")
        .status()
        .unwrap();

    fs::copy("cgo/cgo.h", out_dir.join("cgo.h")).unwrap();

    bindgen::Builder::default()
        .header(header_path.to_string_lossy())
        .new_type_alias("go_.*")
        .generate()
        .unwrap()
        .write_to_file(bindings_path)
        .unwrap();
}
