package main

// #include "cgo.h"
import "C"

import "fmt"

//export go_fmt_Println
func go_fmt_Println(s string) (int, error) {
	return fmt.Println(s)
}
