package main

// #include "cgo.h"
import "C"

import "runtime"

//export go_runtime_GC
func go_runtime_GC() {
	runtime.GC()
}

//export go_runtime_GOOS
func go_runtime_GOOS() *C.char {
	return C.CString(runtime.GOOS)
}
