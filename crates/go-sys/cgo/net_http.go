package main

// #include "cgo.h"
import "C"

import (
	"net/http"
	"runtime/cgo"
)

//export go_net_http_DefaultClient
func go_net_http_DefaultClient() C.go_net_http_Client {
	return C.go_net_http_Client(cgo.NewHandle(http.DefaultClient))
}

//export go_net_http_Get
func go_net_http_Get(url string) (C.go_net_http_Response, C.go_error) {
	resp, err := http.Get(url)
	if err != nil {
		return 0, C.go_error(cgo.NewHandle(err))
	}

	return C.go_net_http_Response(cgo.NewHandle(resp)), 0
}
