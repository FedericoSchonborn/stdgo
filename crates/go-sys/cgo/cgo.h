#pragma once

typedef __UINTPTR_TYPE__ uintptr_t;

typedef uintptr_t go_error;
typedef uintptr_t go_net_http_Client;
typedef uintptr_t go_net_http_Response;
typedef uintptr_t go_time_Time;
