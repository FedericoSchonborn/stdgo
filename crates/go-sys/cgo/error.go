package main

// #include "cgo.h"
import "C"

import (
	"runtime/cgo"
)

//export go_error_Error
func go_error_Error(self C.go_error) *C.char {
	return C.CString(cgo.Handle(self).Value().(error).Error())
}
