package main

// #include "cgo.h"
import "C"

import (
	"runtime/cgo"
	"time"
)

//export go_time_Now
func go_time_Now() C.go_time_Time {
	return C.go_time_Time(cgo.NewHandle(time.Now()))
}

//export go_time_Time_Day
func go_time_Time_Day(self C.go_time_Time) int {
	return cgo.Handle(self).Value().(time.Time).Day()
}

//export go_time_Time_String
func go_time_Time_String(self C.go_time_Time) *C.char {
	return C.CString(cgo.Handle(self).Value().(time.Time).String())
}

//export go_time_Time_Date
func go_time_Time_Date(self C.go_time_Time) (year int, month int, day int) {
	_year, _month, _day := cgo.Handle(self).Value().(time.Time).Date()
	return _year, int(_month), _day
}

//export go_time_Time_Sub
func go_time_Time_Sub(self C.go_time_Time, other C.go_time_Time) int64 {
	return int64(cgo.Handle(self).Value().(time.Time).Sub(cgo.Handle(other).Value().(time.Time)))
}

//export go_time_Time_Format
func go_time_Time_Format(self C.go_time_Time, layout string) *C.char {
	return C.CString(cgo.Handle(self).Value().(time.Time).Format(layout))
}
