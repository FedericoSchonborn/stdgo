package main

import "C"

import "runtime/cgo"

//export cgo_Handle_Delete
func cgo_Handle_Delete(self uintptr) {
	cgo.Handle(self).Delete()
}
