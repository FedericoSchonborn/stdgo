use stdgo::{fmt, time};

fn main() {
    fmt::println(&format!(
        "Today's date is {}!",
        time::Time::now().format("Monday January 2, 2006"),
    ));
}
