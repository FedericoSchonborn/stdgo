use std::ffi::CStr;

use go_sys::*;

pub fn goos<'a>() -> &'a str {
    unsafe { CStr::from_ptr(go_runtime_GOOS()).to_str().unwrap() }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(target_os = "linux")]
    #[test]
    fn goos_linux() {
        assert_eq!(goos(), "linux");
    }
}
