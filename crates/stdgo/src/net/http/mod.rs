use go_sys::*;

use crate::{Error, Result};

pub fn get(url: &str) -> Result<Response> {
    unsafe {
        let go_net_http_Get_return { r0: resp, r1: err } = go_net_http_Get(GoString {
            p: url.as_ptr() as *const i8,
            n: url.len() as isize,
        });
        if err.0 != 0 {
            Err(Error(err))
        } else {
            Ok(Response(resp))
        }
    }
}

#[derive(Debug)]
pub struct Client(pub(crate) go_net_http_Client);

impl Default for Client {
    fn default() -> Self {
        Self(unsafe { go_net_http_DefaultClient() })
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        unsafe { cgo_Handle_Delete(self.0 .0 as u64) }
    }
}

#[derive(Debug)]
pub struct Response(pub(crate) go_net_http_Response);

impl Drop for Response {
    fn drop(&mut self) {
        unsafe { cgo_Handle_Delete(self.0 .0 as u64) }
    }
}
