use std::{
    ffi::CStr,
    fmt::{self, Display, Formatter},
};

use go_sys::*;

#[derive(Debug)]
pub struct Time(pub(crate) go_time_Time);

impl Time {
    pub fn now() -> Self {
        Time(unsafe { go_time_Now() })
    }

    pub fn date(&self) -> (isize, isize, isize) {
        let go_time_Time_Date_return {
            r0: year,
            r1: month,
            r2: day,
        } = unsafe { go_time_Time_Date(self.0) };

        (year as isize, month as isize, day as isize)
    }

    pub fn format(&self, layout: &str) -> &str {
        unsafe {
            CStr::from_ptr(go_time_Time_Format(
                self.0,
                GoString {
                    p: layout.as_ptr() as *const i8,
                    n: layout.len() as isize,
                },
            ))
            .to_str()
            .unwrap()
        }
    }
}

impl Display for Time {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", unsafe {
            CStr::from_ptr(go_time_Time_String(self.0))
                .to_str()
                .unwrap()
        })
    }
}

impl Drop for Time {
    fn drop(&mut self) {
        unsafe { cgo_Handle_Delete(self.0 .0 as u64) }
    }
}
