use go_sys::{go_fmt_Println, GoString};

pub fn println(s: &str) /* TODO: -> Result<?> */
{
    unsafe {
        go_fmt_Println(GoString {
            p: s.as_ptr() as *const i8,
            n: s.len() as isize,
        });
    }
}
