pub mod fmt;
pub mod net;
pub mod runtime;
pub mod time;

pub type Result<T, E = Error> = std::result::Result<T, E>;

use std::{
    ffi::CStr,
    fmt::{self as _fmt, Display, Formatter},
};

use go_sys::{cgo_Handle_Delete, go_error, go_error_Error};

#[derive(Debug)]
pub struct Error(pub(crate) go_error);

impl std::error::Error for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> _fmt::Result {
        write!(f, "{}", unsafe {
            CStr::from_ptr(go_error_Error(self.0)).to_str().unwrap()
        })
    }
}

impl Drop for Error {
    fn drop(&mut self) {
        unsafe { cgo_Handle_Delete(self.0 .0 as u64) }
    }
}

unsafe impl Send for Error {}
unsafe impl Sync for Error {}
